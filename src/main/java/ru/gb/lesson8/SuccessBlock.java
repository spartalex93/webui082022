package ru.gb.lesson8;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.gb.lesson6.BaseView;

import static com.codeborne.selenide.Selenide.$;

public class SuccessBlock {
    private SelenideElement totalSumma = $(By.xpath("//span[@class='ajax_block_cart_total']"));

    @Step("Проверяем итоговую сумму заказа")
    public void checkTotalSumma(String expectedSumma) {
        totalSumma.shouldHave(Condition.text(expectedSumma));
    }
}
