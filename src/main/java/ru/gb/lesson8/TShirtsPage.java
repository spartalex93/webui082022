package ru.gb.lesson8;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Allure;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.gb.lesson6.BaseView;

import java.util.List;

import static com.codeborne.selenide.Selenide.*;

public class TShirtsPage {
    private ElementsCollection sizesList = $$(By.xpath("//span[.='Size']/ancestor::div[@class='layered_filter']//a"));

    @Step("Выбрать размер")
    public TShirtsPage selectSize(String size) {
        sizesList.findBy(Condition.text(size)).click();
        return this;
    }

    private SelenideElement leftPriceSliderElement = $(By.xpath("//div[contains(@class,'slider')]//a[1]"));

    @Step("Сдвинуть слайдер цены вправо")
    public TShirtsPage moveLeftPriceSliderElement(int pixelsCount) {
        Selenide.actions()
                .clickAndHold(leftPriceSliderElement)
                .moveByOffset(pixelsCount, 0)
                .perform();
        return this;
    }

    private ElementsCollection dressesList = $$(By.xpath("//div[@class='product-container']"));

    private SelenideElement addToCartButton = $(By.xpath("//span[.='Add to cart']"));

    @Step("Добавить товар в корзину по имени")
    public SuccessBlock addToCartByName(String tshirtName) {
        dressesList.findBy(Condition.text(tshirtName)).hover();
        addToCartButton.click();
        return page(SuccessBlock.class);
    }
}
