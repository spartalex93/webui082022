package ru.gb.lesson8;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.gb.lesson6.BaseView;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;

public class NavigationBlock {
    private SelenideElement womenButton = $(By.xpath("//li/a[.='Women']"));

    private SelenideElement tShirtsButtonInSubmenu = $(By.xpath("//ul[contains(@class, 'submenu')]//a[.='T-shirts']"));

    @Step("Навести курсор мыши на Women и кликнуть на раздел TShirts")
    public TShirtsPage hoverWomenMenuAndClickTShirts() {
        womenButton.hover();
        tShirtsButtonInSubmenu.click();
        return page(TShirtsPage.class);
    }
}
