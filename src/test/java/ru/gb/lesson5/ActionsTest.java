package ru.gb.lesson5;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.ArrayList;

public class ActionsTest {
    WebDriver driver;
    WebDriverWait webDriverWait;
    Actions actions;
    String address;

    @BeforeAll
    static void registerDriver() {
        WebDriverManager.chromedriver().setup();
    }

    @BeforeEach
    void setupBrowser() {
        driver = new ChromeDriver();
        webDriverWait = new WebDriverWait(driver, Duration.ofSeconds(5));
        actions = new Actions(driver);
    }

    @Test
    void highlightTextTest() throws InterruptedException {
        driver.manage().window().setSize(new Dimension(1200, 1200));
        driver.get("https://translate.google.com/?sl=ru&tl=en&text=test&op=translate");
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//textarea[@aria-label='Исходный текст']/following-sibling::div/span")));
        actions.moveToElement(driver.findElement(By.xpath("//textarea[@aria-label='Исходный текст']/following-sibling::div/span")), -20, 0)
                .clickAndHold()
                .moveByOffset(30,0)
                .perform();
        Thread.sleep(5000);
    }

    @Test
    void yetNewExamples() throws InterruptedException {
        driver.get("https://google.com");
        ((JavascriptExecutor)driver).executeScript("alert('sdfdsf')");
        Thread.sleep(5000);
        driver.switchTo().alert().accept();
        Thread.sleep(5000);

        driver.switchTo().newWindow(WindowType.TAB);
        Thread.sleep(2000);

        ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));

        driver.get("https://ya.ru");
        Thread.sleep(2000);
        driver.switchTo().window(tabs.get(0));
        driver.close();
    }

    @AfterEach
    void killBrowser() {
        driver.quit();
    }
}
