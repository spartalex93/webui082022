import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import io.cucumber.java.After;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import ru.gb.lesson8.*;

import static com.codeborne.selenide.Selenide.open;

public class MyStepdefs {
    @Given("^Пользователь авторизован на сайте$")
    public void пользовательАвторизованНаСайте() {
        Configuration.timeout = 15000;
        open("http://automationpractice.com/");
        new MainPage().clickSingInButton();
        new LoginPage().login("spartalex93@test.test", "123456");
    }

    @When("^Навести курсор мыши на раздел Women и кликнуть на TShirts$")
    public void навестиКурсорМышиНаРазделWomen() {
        new MyAccountPage().navigationBlock.hoverWomenMenuAndClickTShirts();
    }

    @And("^Выбрать размер S$")
    public void выбратьРазмерS() {
        new TShirtsPage().selectSize("S");
    }

    @And("^Установить сумму товара$")
    public void установитьСуммуТовара() {
        new TShirtsPage().moveLeftPriceSliderElement(10);
    }

    @And("^Добавить в корзину товар по имени Faded$")
    public void добавитьВКорзинуТоварПоИмениFaded() {
        new TShirtsPage().addToCartByName("Faded");
    }

    @io.cucumber.java.en.Then("^Проверить сумму заказа$")
    public void проверитьСуммуЗаказа() {
        new SuccessBlock().checkTotalSumma("$18.51");
    }

    @And("Выбрать размер {string}")
    public void выбратьРазмерSize(String size) {
        new TShirtsPage().selectSize(size);
    }

    @After(value = "@closeBrowser")
    public void closeBrowser() {
        Selenide.closeWebDriver();
    }

}
