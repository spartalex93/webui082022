Feature: Добавление товара в корзину

  Background:
    Given Пользователь авторизован на сайте

    @hook
      @closeBrowser
  Scenario Outline:
    When Навести курсор мыши на раздел Women и кликнуть на TShirts
    And Выбрать размер '<size>'
    And Установить сумму товара
    And Добавить в корзину товар по имени Faded
    Then Проверить сумму заказа
    Examples:
      | size |
      | S    |
      | M    |